package com.dashingdiva.salon;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dashingdiva.salon.fragment.Book_Appointment_Fragment;
import com.dashingdiva.salon.fragment.Contactus_Fragment;
import com.dashingdiva.salon.fragment.Pager_Fragment;
import com.dashingdiva.salon.fragment.Services_Fragment;
import com.dashingdiva.salon.utils.Constants;
import com.dashingdiva.salon.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.sephiroth.android.library.bottomnavigation.BottomNavigation;

public class Splash extends AppCompatActivity{

    TextView tv_her,tv_him,tv_enterher,tv_enterhim;
    int  REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS=124;
    SharedPreferences  pref;
    SharedPreferences.Editor edit;
    JSONArray men_array,women_array;
    NetworkConnection nw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        tv_her=(TextView)findViewById(R.id.tv_servicefor_her);
        tv_him=(TextView)findViewById(R.id.tv_servicefor_him);
        tv_enterher=(TextView)findViewById(R.id.tv_enter);
        tv_enterhim=(TextView)findViewById(R.id.tv_enter1);

        Typeface tf1 = Typeface.createFromAsset(getAssets(), "MISTRAL.TTF");
        tv_her.setTypeface(tf1);
        tv_him.setTypeface(tf1);
        tv_enterher.setTypeface(tf1);
        tv_enterhim.setTypeface(tf1);

        pref=getSharedPreferences("pref",0);

        nw=new NetworkConnection(Splash.this);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            getEnablePermission();
        }

//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                Intent i = new Intent(Splash.this, HomeActivity.class);
//                startActivity(i);
//                finish();
//            }
//        }, 3000);

        if(Constants.women_array!=null&&Constants.men_array!=null) {
            if (Constants.women_array.length() > 0 & Constants.men_array.length() > 0) {
            } else
                getBanner();
        }
        else
        getBanner();

        tv_enterher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(nw.isConnectingToInternet()) {
                        if (Constants.women_array.length() > 0) {
                            edit = pref.edit();

                            Constants.Gender = "women";

                            edit.putString("gender", "women");

                            edit.commit();

                            Intent i = new Intent(Splash.this, HomeActivity.class);
                            startActivity(i);
                            finish();
                        } else
                            Toast.makeText(Splash.this, "Please Wait..", Toast.LENGTH_LONG).show();
                }
                else
                    alertDialog();

        }
        });

        tv_enterhim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(nw.isConnectingToInternet()) {
                    if (Constants.men_array.length() > 0) {
                        edit = pref.edit();

                        edit.putString("gender", "men");

                        edit.commit();

                        Constants.Gender = "men";
                        Intent i = new Intent(Splash.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    } else
                        Toast.makeText(Splash.this, "Please Wait..", Toast.LENGTH_LONG).show();
                }
                else
                    alertDialog();
            }
        });
    }

    private void getEnablePermission() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
//        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
//            permissionsNeeded.add("INTERNET");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("ACCESS_FINE_LOCATION");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("ACCESS_COARSE_LOCATION");
//        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
//            permissionsNeeded.add("READ_EXTERNAL_STORAGE");
//        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
//            permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");

        if (permissionsList.size() > 0) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    private void getBanner() {
        // TODO Auto-generated method stub

        final ProgressDialog p = new ProgressDialog(Splash.this);
        p.setCancelable(true);
        p.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        p.show();

        String url= Constants.URL+"get_banner1.php";
        Log.e("url", url);
        //   Constants.array=new JSONArray();
        RequestQueue que= Volley.newRequestQueue(Splash.this);

        StringRequest obj1=new StringRequest(com.android.volley.Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub
                p.dismiss();

                Log.e("profile_response",res);
                int size=res.length();
                if(size>0)
                {

                    try {

                        JSONObject arg0=new JSONObject(res);

                        String respon= arg0.getString("response_men");
                        String respon1= arg0.getString("response_women");
                        if(respon.equalsIgnoreCase("success"))
                        {
                            Constants.men_array=arg0.getJSONArray("banner_men");
                        }

                        if(respon1.equalsIgnoreCase("success"))
                        {
                            Constants.women_array=arg0.getJSONArray("banner_women");
                        }

                        else if(respon.equalsIgnoreCase("no services")){

                        }
                        else if(respon.equalsIgnoreCase("connection error")){

                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        p.dismiss();
                        Log.e("token_exception",e.toString());
                        e.printStackTrace();
                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                p.dismiss();
                Log.e("token_error",e.toString());
                if(!nw.isConnectingToInternet())
                    alertDialog();
            }
        });

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    public void alertDialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Splash.this);
        alertDialogBuilder.setMessage("No Internet Connection");
        alertDialogBuilder.setPositiveButton("Refresh", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getBanner();
            }
        });

//        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.cancel();
//
//            }
//        });

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();
    }

}
