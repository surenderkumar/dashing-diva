package com.dashingdiva.salon;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.IdRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dashingdiva.salon.fragment.Book_Appointment_Fragment;
import com.dashingdiva.salon.fragment.Contactus_Fragment;
import com.dashingdiva.salon.fragment.Pager_Fragment;
import com.dashingdiva.salon.fragment.Services_Fragment;
import com.dashingdiva.salon.utils.Constants;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import it.sephiroth.android.library.bottomnavigation.BottomNavigation;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    public static TextView title;
    public static ImageView logo;
    FrameLayout frameLayout;
    BottomNavigation navigation_bar;

    TextView tab_home,tab_services,tab_bookappointment,tab_contactus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        title=(TextView)findViewById(R.id.tv_title);
        logo=(ImageView)findViewById(R.id.iv_logo);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        frameLayout=(FrameLayout)findViewById(R.id.framelayout);

        tab_home=(TextView)findViewById(R.id.tv_home);

        tab_services=(TextView)findViewById(R.id.tv_service);
        tab_bookappointment=(TextView)findViewById(R.id.tv_bookappointment);

        tab_contactus=(TextView)findViewById(R.id.tv_contactus);

        Typeface tf = Typeface.createFromAsset(getAssets(), "MISTRAL.TTF");
        Typeface tf1 = Typeface.createFromAsset(getAssets(), "Aller_Std_Lt.ttf");
        title.setTypeface(tf);

        tab_home.setTypeface(tf1);
        tab_services.setTypeface(tf1);
        tab_bookappointment.setTypeface(tf1);
        tab_contactus.setTypeface(tf1);


//        navigation_bar=(BottomNavigation)findViewById(R.id.BottomNavigation);
//        navigation_bar.setOnMenuItemClickListener(this);


        Pager_Fragment homeFragment = new Pager_Fragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,homeFragment).addToBackStack(null).commit();

        tab_home.setOnClickListener(this);
        tab_services.setOnClickListener(this);
        tab_bookappointment.setOnClickListener(this);
        tab_contactus.setOnClickListener(this);

    }

//    @Override
//    public void onMenuItemSelect(@IdRes int i, int i1, boolean b) {
//
//        switch (i)
//        {
//            case R.id.home:
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new Pager_Fragment()).addToBackStack(null).commit();
//                break;
//            case R.id.contact:
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new Services_Fragment()).addToBackStack(null).commit();
//                break;
//            case R.id.downline:
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new Book_Appointment_Fragment()).addToBackStack(null).commit();
//                break;
//            case R.id.activity:
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new Contactus_Fragment()).addToBackStack(null).commit();
//                break;
//        }
//
//
//    }
//
//    @Override
//    public void onMenuItemReselect(@IdRes int i, int i1, boolean b) {
//
//    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();

        if(Constants.checkfragment==0)
            exitdialog();
        else if(Constants.checkfragment==5){
            Services_Fragment homeFragment = new Services_Fragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,homeFragment).commit();
        }
        else{
            Pager_Fragment homeFragment = new Pager_Fragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,homeFragment).commit();
        }
//            super.onBackPressed();

    }

    public void exitdialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Do you really want to exit?");

        alertDialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1)

            {
                finish();
                //moveTaskToBack(true);
                // Toast.makeText(Jobsandalert_activity.this,"You clicked yes button",Toast.LENGTH_LONG).show();
            }

        });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();


    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.tv_home){


            Intent in = new Intent(HomeActivity.this,Splash.class);
            startActivity(in);
            finish();
//            if(Constants.checkfragment!=1)
//            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new Pager_Fragment()).commit();

        }
        else if(v.getId()==R.id.tv_service){

            if(Constants.checkfragment!=2)
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new Services_Fragment()).commit();

        }
        else if(v.getId()==R.id.tv_bookappointment){

            if(Constants.checkfragment!=3)
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new Book_Appointment_Fragment()).commit();

        }
        else if(v.getId()==R.id.tv_contactus){

            if(Constants.checkfragment!=4)
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,new Contactus_Fragment()).addToBackStack(null).commit();

        }
    }
}
