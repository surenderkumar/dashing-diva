package com.dashingdiva.salon.getset;

/**
 * Created by surender on 4/10/2017.
 */
public class sub_service_getset {

    String id,postid,sub_ser_name,price,time;


    public sub_service_getset(String id, String postid, String sub_ser_name, String price, String time) {
        this.id = id;
        this.postid = postid;
        this.sub_ser_name = sub_ser_name;
        this.price = price;
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public String getSub_ser_name() {
        return sub_ser_name;
    }

    public void setSub_ser_name(String sub_ser_name) {
        this.sub_ser_name = sub_ser_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
