package com.dashingdiva.salon.getset;

/**
 * Created by surender on 4/10/2017.
 */
public class service_getset {

    String id,genger,servicename,image;

    public service_getset(String id, String genger, String servicename, String image) {
        this.id = id;
        this.genger = genger;
        this.servicename = servicename;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGenger() {
        return genger;
    }

    public void setGenger(String genger) {
        this.genger = genger;
    }

    public String getServicename() {
        return servicename;
    }

    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
