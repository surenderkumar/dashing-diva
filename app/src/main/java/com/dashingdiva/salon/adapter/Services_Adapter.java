package com.dashingdiva.salon.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dashingdiva.salon.R;
import com.dashingdiva.salon.fragment.Sub_Services_Fragment;
import com.dashingdiva.salon.utils.AnimateFirstDisplayListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by surender on 4/5/2017.
 */
public class Services_Adapter extends BaseAdapter {

    Context c;
    JSONArray data;
    JSONObject object;

    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;

    public Services_Adapter(Context c, JSONArray data) {
        this.c = c;
        this.data=data;
        options = new DisplayImageOptions.Builder()
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 0))
                .showImageForEmptyUri(R.drawable.images)
                .showImageOnFail(R.drawable.images)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

    }

    @Override
    public int getCount() {
        return data.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int p, View convertView, ViewGroup parent) {

        c=parent.getContext();

        LayoutInflater inf=(LayoutInflater)c.getSystemService(c.LAYOUT_INFLATER_SERVICE);
        View v=inf.inflate(R.layout.service_list_item,parent,false);

        TextView name=(TextView)v.findViewById(R.id.tv_servicename);
        ImageView iv_service_icon=(ImageView)v.findViewById(R.id.iv_service_item);
        ImageView iv_next=(ImageView)v.findViewById(R.id.iv_next);
        LinearLayout layout=(LinearLayout)v.findViewById(R.id.layout);

        Typeface tf1 = Typeface.createFromAsset(c.getAssets(), "Aller_Std_Lt.ttf");
        name.setTypeface(tf1);

        try {
            object=data.getJSONObject(p);

            name.setText(object.getString("service_name"));

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(c));
            imageLoader.getInstance().displayImage(object.getString("image"), iv_service_icon, options, animateFirstListener);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        object = data.getJSONObject(p);

                        Sub_Services_Fragment homeFragment = new Sub_Services_Fragment();
                        Log.e("service_id",object.getString("id"));
                        Bundle bundle = new Bundle();
                        bundle.putString("id", object.getString("id"));
                        bundle.putString("service_name",object.getString("service_name"));
                        homeFragment.setArguments(bundle);

                        ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,homeFragment).addToBackStack(null).commit();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }




//        ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, basic_frag).
//                addToBackStack(null).commit();

        if(p%2==0){
        layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        else
            layout.setBackgroundColor(Color.parseColor("#f9f9f9"));

        return v;
    }
}
