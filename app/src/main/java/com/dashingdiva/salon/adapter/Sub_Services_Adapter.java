package com.dashingdiva.salon.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dashingdiva.salon.R;
import com.dashingdiva.salon.fragment.Book_Appointment_Fragment;
import com.dashingdiva.salon.fragment.Services_Fragment;
import com.dashingdiva.salon.fragment.Sub_Services_Fragment;
import com.dashingdiva.salon.utils.AnimateFirstDisplayListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by surender on 4/5/2017.
 */
public class Sub_Services_Adapter extends BaseAdapter {

    Context c;
    JSONArray data;
    JSONObject object;

    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;

    public Sub_Services_Adapter(Context c,JSONArray data) {
        this.c = c;
        this.data=data;
        options = new DisplayImageOptions.Builder()
                .displayer(new CircleBitmapDisplayer(Color.BLUE, 4))
                .showImageForEmptyUri(R.drawable.logo_female)
                .showImageOnFail(R.drawable.logo_female)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

    }

    @Override
    public int getCount() {
        return data.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int p, View convertView, ViewGroup parent) {

        c=parent.getContext();

        LayoutInflater inf=(LayoutInflater)c.getSystemService(c.LAYOUT_INFLATER_SERVICE);
        View v=inf.inflate(R.layout.sub_service_list_item,parent,false);

        LinearLayout layout=(LinearLayout)v.findViewById(R.id.layout);
        TextView tv_name=(TextView)v.findViewById(R.id.tv_servicename);
        TextView tv_price=(TextView)v.findViewById(R.id.tv_price);
        TextView tv_time=(TextView)v.findViewById(R.id.tv_time);
        TextView tv_book=(TextView)v.findViewById(R.id.tv_book);
        LinearLayout lay_book=(LinearLayout)v.findViewById(R.id.lay_book);

        Typeface tf1 = Typeface.createFromAsset(c.getAssets(), "Aller_Std_Lt.ttf");
        Typeface tf = Typeface.createFromAsset(c.getAssets(), "Aller_Std_Rg.ttf");
        tv_name.setTypeface(tf1);
        tv_price.setTypeface(tf);
        tv_time.setTypeface(tf);
        tv_book.setTypeface(tf);

        try {
            object=data.getJSONObject(p);
            tv_name.setText(object.getString("service_name"));
            tv_price.setText(object.getString("price")+" Tsh");
            tv_time.setText(object.getString("time")+" Mins");

            lay_book.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        object = data.getJSONObject(p);

                        Book_Appointment_Fragment homeFragment = new Book_Appointment_Fragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("postid", Sub_Services_Fragment.postid);
                        bundle.putString("id", object.getString("id"));
                        bundle.putString("service_name", Sub_Services_Fragment.service_name);
                        bundle.putString("subservice_name",object.getString("service_name"));

                        homeFragment.setArguments(bundle);

                        ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,homeFragment).addToBackStack(null).commit();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(p%2==0){
        layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        else
            layout.setBackgroundColor(Color.parseColor("#e7e7e7"));

//        ImageLoader imageLoader = ImageLoader.getInstance();
//        imageLoader.init(ImageLoaderConfiguration.createDefault(c));
//        imageLoader.getInstance().displayImage("url", iv_service_icon, options, animateFirstListener);

        return v;
    }
}
