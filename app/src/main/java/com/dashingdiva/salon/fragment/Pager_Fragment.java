package com.dashingdiva.salon.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dashingdiva.salon.HomeActivity;
import com.dashingdiva.salon.R;
import com.dashingdiva.salon.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class Pager_Fragment extends Fragment {

    ViewPager viewPager;
    View v;
    TimerTask timer;
    int page=0;
    private Handler handler;
    ImageView iv_logo;
    TextView tv_relax,tv_athome,tv_servicename,tv_allservice,tv_phone;
    SharedPreferences pref;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.home_pager_fragment, container, false);

        Constants.checkfragment=0;
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_icon);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        HomeActivity.title.setVisibility(View.GONE);
        HomeActivity.logo.setVisibility(View.VISIBLE);
        HomeActivity.title.setText("Services");
        handler = new Handler();
        pref=getActivity().getSharedPreferences("pref",0);

        if(pref.getString("gender","women").equalsIgnoreCase("men"))
            HomeActivity.logo.setImageResource(R.drawable.logo_white_male);

//        tv_relax=(TextView)v.findViewById(R.id.tv_relax);
//        tv_athome=(TextView)v.findViewById(R.id.tv_athome);
//        tv_servicename=(TextView)v.findViewById(R.id.tv_servicesname);
//        tv_allservice=(TextView)v.findViewById(R.id.tv_allservices);
//        tv_phone=(TextView)v.findViewById(R.id.tv_phone);
//        iv_logo=(ImageView)v.findViewById(R.id.iv_logo);

//        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Aller_Std.ttf");
//        Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(), "Aller_Std_Lt.ttf");
//        Typeface tf2 = Typeface.createFromAsset(getActivity().getAssets(), "Aller_Std_Rg.ttf");
//        tv_relax.setTypeface(tf1);
//        tv_athome.setTypeface(tf);
//
//        tv_allservice.setTypeface(tf);
//
//        tv_servicename.setTypeface(tf2);
//        tv_phone.setTypeface(tf2);

//        if(Constants.Gender.equalsIgnoreCase("men"))
//            iv_logo.setImageResource(R.drawable.logo_male);
//        else
//            iv_logo.setImageResource(R.drawable.logo_female);

        //getBanner();

         //  initViewPager();

        if(Constants.Gender.equals("women"))
        initViewPager(Constants.women_array);
        else
        initViewPager(Constants.men_array);

        return v;
    }

    private void initViewPager(JSONArray array) {

        Log.e("starting","starting");
        viewPager = (ViewPager)v.findViewById(R.id.viewPager);

        final PagerAdapter pagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager());

        for(int i=0;i<array.length();i++){
            Log.e("starting "+i,"starting");

            try {
                JSONObject obj=array.getJSONObject(i);

                Pager_Image_Fragment frag=new Pager_Image_Fragment();
                Bundle bundle = new Bundle();
                bundle.putString("url", obj.getString("image"));
                bundle.putString("imageid",obj.getString("id"));
                frag.setArguments(bundle);
                pagerAdapter.addFragment(frag, " ");

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("page_exc","e",e);
            }
        }


//        pagerAdapter.addFragment(new SplashFragment2(), "Features");
//        pagerAdapter.addFragment(new Filter_sliding_Fragment1(), "Features");
//        pagerAdapter.addFragment(new SplashFragment3(),"Features");

        CircleIndicator indicator = (CircleIndicator)v.findViewById(R.id.indicator);

        viewPager.setAdapter(pagerAdapter);
        indicator.setViewPager(viewPager);
        pagerAdapter.notifyDataSetChanged();

        timer=new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        viewPager.setCurrentItem(page % 4);

                        page++;
                    }
                });
            }
        };
        Timer time=new Timer();
        time.schedule(timer, 0, 3000);

    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    private void getBanner() {
        // TODO Auto-generated method stub

        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setCancelable(true);
        p.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        p.show();

        String url= Constants.URL+"get_banner.php";
        Log.e("url", url);
     //   Constants.array=new JSONArray();
        RequestQueue que= Volley.newRequestQueue(getActivity());

        StringRequest obj1=new StringRequest(com.android.volley.Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub
                p.dismiss();

                Log.e("profile_response",res);
                int size=res.length();
                if(size>0)
                {

                    try {

                        JSONObject arg0=new JSONObject(res);

                        String respon= arg0.getString("response");
                        if(respon.equalsIgnoreCase("success"))
                        {
                            JSONArray array=arg0.getJSONArray("banner");

                            if(array.length()>0){
                             initViewPager(array);
                            }
                        }
                        else if(respon.equalsIgnoreCase("no services")){

                        }
                        else if(respon.equalsIgnoreCase("connection error")){

                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        p.dismiss();
                        Log.e("token_exception",e.toString());
                        e.printStackTrace();
                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                p.dismiss();
                Log.e("token_error",e.toString());
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("gender",pref.getString("gender","women"));
                return params;
            }
        };

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

}
