package com.dashingdiva.salon.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dashingdiva.salon.HomeActivity;
import com.dashingdiva.salon.Manifest;
import com.dashingdiva.salon.R;
import com.dashingdiva.salon.adapter.SpinnerAdapter;
import com.dashingdiva.salon.utils.Constants;
import com.dashingdiva.salon.utils.GPSTracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by surender on 4/5/2017.
 */
public class Contactus_Fragment extends Fragment implements OnMapReadyCallback {

//    EF:83:ED:13:09:A4:78:CC:78:19:55:E1:88:51:9A:6E:50:17:BD:3A

    View v;
    private GoogleMap googleMap;
    ImageView iv_logo;
    LinearLayout layout;
    TextView tv_address, tv_email, tv_website, tv_phone, tv_direction;
    ImageView iv_fb,iv_instagram,iv_flag;
    private Marker marker;
    double lat = 29.1492;
    double lng = 75.7217;
    double cur_lat = 29.1492;
    double cur_lng = 75.7217;
    int  REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS=124;
    GPSTracker gpsTracker;
    String Address="",Title = "Dashing Diva";
    SharedPreferences pref;
    int userIcon = R.drawable.marker;
    String fb_link="",instagram_link="",flag_link="";

    private void setUpMapIfNeeded() {
        if (googleMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

            mapFrag.getMapAsync(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.contact_us, container, false);

        Constants.checkfragment=4;

        pref=getActivity().getSharedPreferences("pref",0);

        if(pref.getString("gender","women").equalsIgnoreCase("men"))
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_for_him);
        else
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_icon);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        HomeActivity.title.setVisibility(View.VISIBLE);
        HomeActivity.logo.setVisibility(View.GONE);
        HomeActivity.title.setText("Contact Us   ");

        iv_logo=(ImageView)v.findViewById(R.id.iv_logo);

        if(pref.getString("gender","women").equalsIgnoreCase("men"))
            iv_logo.setImageResource(R.drawable.logo_the_hair_lounge1);

        gpsTracker = new GPSTracker(getActivity());


        tv_address = (TextView) v.findViewById(R.id.tv_address);
        tv_email = (TextView) v.findViewById(R.id.tv_email);
        tv_website = (TextView) v.findViewById(R.id.tv_website);
        tv_phone = (TextView) v.findViewById(R.id.tv_phone);
        tv_direction = (TextView) v.findViewById(R.id.tv_direction);

        iv_fb=(ImageView)v.findViewById(R.id.iv_facebook);
        iv_instagram=(ImageView)v.findViewById(R.id.iv_instagram);
        iv_flag=(ImageView)v.findViewById(R.id.iv_flag);
        layout=(LinearLayout)v.findViewById(R.id.layout);

        Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(), "Aller_Std_Rg.ttf");
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "MISTRAL.TTF");
        tv_address.setTypeface(tf1);
        tv_website.setTypeface(tf1);
        tv_phone.setTypeface(tf1);
        tv_email.setTypeface(tf1);
        tv_direction.setTypeface(tf);

        tv_address.setText("");
        tv_website.setText("");
        tv_phone.setText("");
        tv_email.setText("");

        get_Address();


        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkGPS();

                lat=29.3642;
                lng=75.9057;

//                String uri = "http://maps.google.com/";
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                startActivity(intent);

            }
        });


        iv_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(URLIsReachable(fb_link)){
                    Intent in = new Intent(Intent.ACTION_VIEW);
                    in.setData(Uri.parse(fb_link));
                    startActivity(in);
                }
            }
        });

        iv_instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(URLIsReachable(instagram_link)){
                    Intent in = new Intent(Intent.ACTION_VIEW);
                    in.setData(Uri.parse(instagram_link));
                    startActivity(in);
                }
            }
        });

        iv_flag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(URLIsReachable(flag_link)){
                    Intent in = new Intent(Intent.ACTION_VIEW);
                    in.setData(Uri.parse(flag_link));
                    startActivity(in);
                }
            }
        });


        return v;
    }

    public boolean URLIsReachable(String urlString)
    {

        try
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();
            int responseCode = urlConnection.getResponseCode();
            urlConnection.disconnect();
            // Toast.makeText(c,"code= "+responseCode,Toast.LENGTH_LONG).show();
            if(responseCode==200)
                return true;
            else
                return false;
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
            return false;
        } catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public  void checkGPS(){

        if (gpsTracker.getIsGPSTrackingEnabled()) {

            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                getEnablePermission();

                return;
            }

            cur_lat = gpsTracker.getLatitude();

            cur_lng = gpsTracker.getLongitude();

            Log.e("valueeeeee", cur_lat + " " + cur_lng );

            Constants.checkfragment=6;
            String uri = "http://maps.google.com/maps?f=d&hl=en&saddr="+cur_lat+","+cur_lng+"&daddr="+lat+","+lng;
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(Intent.createChooser(intent, "Select an application"));

//            Log.e("valueeeeee", stringLatitude + " " + stringLongitude + " " + country + " " + city + " " + postalCode);

        }
        else {
            gpsTracker.showSettingsAlert();

            return;
        }


    }




    private void get_Address() {
        // TODO Auto-generated method stub

        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setCancelable(true);
        p.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        //   p.show();

        String url = Constants.URL + "get_Contactus.php";
        Log.e("url", url);
        //   Constants.array=new JSONArray();
        RequestQueue que = Volley.newRequestQueue(getActivity());

        StringRequest obj1 = new StringRequest(com.android.volley.Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub
                p.dismiss();

                Log.e("profile_response", res);
                int size = res.length();
                if (size > 0) {

                    try {

                        JSONObject arg0 = new JSONObject(res);

                        String respon = arg0.getString("response");
                        if (respon.equalsIgnoreCase("success")) {
                            JSONArray array = arg0.getJSONArray("address");
                            if (array.length() > 0) {
                                JSONObject obj = array.getJSONObject(0);

                                setData(obj);

                            }

                        } else if (respon.equalsIgnoreCase("no address")) {

                        } else if (respon.equalsIgnoreCase("connection error")) {

                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        p.dismiss();
                        Log.e("token_exception", e.toString());
                        e.printStackTrace();
                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                p.dismiss();
                Log.e("token_error", e.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("gender", pref.getString("gender","women"));
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    public void setData(JSONObject obj) {

        try {

            Address=obj.getString("address");
            lat= Double.parseDouble(obj.getString("lat"));
            lng= Double.parseDouble(obj.getString("lng"));

            tv_address.setText(Address);
            tv_website.setText(obj.getString("website"));
            tv_phone.setText(obj.getString("phone"));
            tv_email.setText(obj.getString("email"));

            fb_link=obj.getString("fb_link");
            instagram_link=obj.getString("instagram_link");
            flag_link=obj.getString("other_link");


            setUpMapIfNeeded();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setUpMap() {

        if (marker != null) {
            marker.remove();
        }

        LatLng position = new LatLng(lat, lng);

        MarkerOptions markerOptions = new MarkerOptions().
                position(position).
                title(Title).
                icon(BitmapDescriptorFactory.fromResource(userIcon)).
                snippet(Address);

        googleMap.addMarker(markerOptions);

//        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
//                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//            return;
//        }
//
//        googleMap.setMyLocationEnabled(true);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, 15);
        googleMap.animateCamera(cameraUpdate);

    }

    @Override
    public void onMapReady(GoogleMap googleMap1) {

        googleMap = googleMap1;
        setUpMap();
    }


    private void getEnablePermission() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
//        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
//            permissionsNeeded.add("INTERNET");
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("ACCESS_FINE_LOCATION");
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("ACCESS_COARSE_LOCATION");
//        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
//            permissionsNeeded.add("READ_EXTERNAL_STORAGE");
//        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
//            permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");

        if (permissionsList.size() > 0) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }
}
