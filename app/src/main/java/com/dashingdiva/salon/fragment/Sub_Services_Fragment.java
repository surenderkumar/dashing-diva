package com.dashingdiva.salon.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dashingdiva.salon.HomeActivity;
import com.dashingdiva.salon.R;
import com.dashingdiva.salon.adapter.Services_Adapter;
import com.dashingdiva.salon.adapter.Sub_Services_Adapter;
import com.dashingdiva.salon.utils.Constants;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by surender on 4/5/2017.
 */
public class Sub_Services_Fragment extends Fragment {

    View v;
    ListView listView;
    TextView tv_message;
    Sub_Services_Adapter adapter;

    //String postid="0";
    public static String service_name,postid="0";

    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v=inflater.inflate(R.layout.services,container,false);

        Constants.checkfragment=5;

        pref=getActivity().getSharedPreferences("pref",0);

        if(pref.getString("gender","women").equalsIgnoreCase("men"))
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_for_him);
        else
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_icon);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        HomeActivity.title.setVisibility(View.VISIBLE);
        HomeActivity.logo.setVisibility(View.GONE);

        listView=(ListView)v.findViewById(R.id.listview);
        tv_message=(TextView)v.findViewById(R.id.tv_message);


        Bundle bundle = this.getArguments();

        if (bundle != null) {

            postid = bundle.getString("id", "0");
            service_name = bundle.getString("service_name", "");
            HomeActivity.title.setText(service_name+"       ");

            Log.e("postid",postid+" postid");
        }

        if(!postid.equalsIgnoreCase("0"))
        getService();

        return v;
    }

    private void getService() {
        // TODO Auto-generated method stub

        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setCancelable(true);
        p.setMessage("Getting Services...");
        p.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
//           p.show();

        String url= Constants.URL+"get_subservices.php";
        Log.e("url", url);
        //   Constants.array=new JSONArray();
        RequestQueue que= Volley.newRequestQueue(getActivity());

        StringRequest obj1=new StringRequest(com.android.volley.Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub
                p.dismiss();

                Log.e("profile_response",res);
                int size=res.length();
                if(size>0)
                {

                    try {

                        JSONObject arg0=new JSONObject(res);

                        String respon= arg0.getString("response");
                        if(respon.equalsIgnoreCase("success"))
                        {

                            final JSONArray array=arg0.getJSONArray("services");

                            if(array.length()>0){

                                listView.setVisibility(View.VISIBLE);
                                tv_message.setVisibility(View.GONE);

                                adapter=new Sub_Services_Adapter(getActivity(),array);
                                listView.setAdapter(adapter);
                            }

                        }
                        else if(respon.equalsIgnoreCase("no services")){

                            listView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                            tv_message.setText("No Data Found");
//                            No Services Update soon
                        }
                        else if(respon.equalsIgnoreCase("connection error")){
                            listView.setVisibility(View.GONE);
                            tv_message.setVisibility(View.VISIBLE);
                            tv_message.setText("No Data Found");//No Services Update soon
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        p.dismiss();
                        Log.e("token_exception",e.toString());
                        e.printStackTrace();

                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                p.dismiss();
                Log.e("token_error",e.toString());
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("postid",postid);
                return params;
            }
        };

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

}
