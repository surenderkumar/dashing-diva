package com.dashingdiva.salon.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dashingdiva.salon.HomeActivity;
import com.dashingdiva.salon.R;
import com.dashingdiva.salon.adapter.ExpandableListAdapter;
import com.dashingdiva.salon.adapter.Services_Adapter;
import com.dashingdiva.salon.adapter.SpinnerAdapter;
import com.dashingdiva.salon.adapter.Sub_Services_Adapter;
import com.dashingdiva.salon.getset.service_getset;
import com.dashingdiva.salon.getset.sub_service_getset;
import com.dashingdiva.salon.utils.Constants;
import com.dashingdiva.salon.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by surender on 4/5/2017.
 */

public class Book_Appointment_Fragment extends Fragment {

    View v;
    ListView listView;
    EditText et_name,et_phone,et_date,et_time,et_message;
    Button bt_booknow;

    TextView tv_service;
    //ArrayList<String> service_list,sub_servicelist;
    JSONArray service_array,subservice_array;
    JSONObject object;
    String serviceid="0",postid="0",service_name="",subservice_name="",st_service_id="",st_subservice_id="";

    String st_name,st_phone,st_date="",st_time="",st_message;
    NetworkConnection nw;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;

    ArrayList<sub_service_getset> sub_service_list;

    ArrayList<service_getset> listDataHeader;
    HashMap<String, ArrayList<sub_service_getset>> listDataChild;

    SharedPreferences pref;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

       v=inflater.inflate(R.layout.book_appointment,container,false);

        Constants.checkfragment=3;

        pref=getActivity().getSharedPreferences("pref",0);

        if(pref.getString("gender","women").equalsIgnoreCase("men"))
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_for_him);
        else
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_icon);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        HomeActivity.title.setVisibility(View.VISIBLE);
        HomeActivity.logo.setVisibility(View.GONE);
        HomeActivity.title.setText("Book Appointment");

        et_name=(EditText)v.findViewById(R.id.et_name);
        et_phone=(EditText)v.findViewById(R.id.et_phone);
       // et_email=(EditText)v.findViewById(R.id.et_email);
        et_date=(EditText)v.findViewById(R.id.et_date);
        et_time=(EditText)v.findViewById(R.id.et_time);
        et_message=(EditText)v.findViewById(R.id.et_message);
        bt_booknow=(Button)v.findViewById(R.id.bt_booknow);
        tv_service=(TextView) v.findViewById(R.id.sp_services);

//        et_date.setEnabled(false);
//        et_time.setEnabled(false);

//        sp_subservice=(Spinner)v.findViewById(R.id.sp_sub_services);

  //      pref=getActivity().getSharedPreferences("pref",0);

        listDataHeader=new ArrayList<>();
        listDataChild=new HashMap<>();

        Typeface tf1 = Typeface.createFromAsset(getActivity().getAssets(), "MISTRAL.TTF");
//        et_name.setTypeface(tf1);
//        et_phone.setTypeface(tf1);
//        et_email.setTypeface(tf1);
//        et_date.setTypeface(tf1);
//        et_time.setTypeface(tf1);
//        et_message.setTypeface(tf1);

        bt_booknow.setTypeface(tf1);

        nw=new NetworkConnection(getActivity());

        Bundle bundle = this.getArguments();

        if (bundle != null) {

            st_subservice_id = bundle.getString("id", "0");

            st_service_id = bundle.getString("postid", "0");

            service_name = bundle.getString("service_name", "");

            subservice_name = bundle.getString("subservice_name", "");

            tv_service.setText(service_name+" - "+subservice_name);

            Log.e("postid",postid+" postid");
        }
        else
        {
            tv_service.setText("Select Service");
        }


        getService();

        et_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        datedilog();
                        break;
                    case MotionEvent.ACTION_UP:

                        break;
                    default:
                        break;
                }
                return true;
            }
        });



        et_time.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        timedilog();
                        break;
                    case MotionEvent.ACTION_UP:

                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        bt_booknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                st_name=et_name.getText().toString();
                st_phone=et_phone.getText().toString();
               // st_email=et_email.getText().toString();
//                st_date=et_date.getText().toString();
//                st_time=et_time.getText().toString();
                st_message=et_message.getText().toString();

                if(st_name.length()<1){
                    Toast.makeText(getActivity(),"Enter Name",Toast.LENGTH_LONG).show();
                    return;
                }
                else if(st_name.length()<4|st_name.length()>25){
                    Toast.makeText(getActivity(),"Name lenght should be 4 to 25 char",Toast.LENGTH_LONG).show();
                    return;
                }
                else if(st_phone.length()!=10){
                    Toast.makeText(getActivity(),"Enter Valid Phone No.",Toast.LENGTH_LONG).show();
                    return;
                }
//                else if(!isValidEmail(st_email)){
//                    Toast.makeText(getActivity(),"Enter Valid Email Address",Toast.LENGTH_LONG).show();
//                    return;
//                }
                else if(service_name.length()<1){
                    Toast.makeText(getActivity(),"You Have No Selected Service",Toast.LENGTH_LONG).show();
                    return;
                }
                else if(st_date.length()<1){
                    Toast.makeText(getActivity(),"Enter Valid Date",Toast.LENGTH_LONG).show();
                    return;
                }
                else if(st_time.length()<1){
                    Toast.makeText(getActivity(),"Enter Valid Time",Toast.LENGTH_LONG).show();
                    return;
                }
                else {
                    Submitbooking();
                }
            }
        });

        tv_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listdilog();
            }
        });

        return v;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void getService() {
        // TODO Auto-generated method stub

        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setCancelable(false);
        p.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
           p.show();

        String url= Constants.URL+"get_services.php";
        Log.e("url", url);
        //   Constants.array=new JSONArray();
        RequestQueue que= Volley.newRequestQueue(getActivity());

        StringRequest obj1=new StringRequest(com.android.volley.Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub
                p.dismiss();

                Log.e("profile_response",res);
                int size=res.length();
                if(size>0)
                {

                    try {

                        JSONObject arg0=new JSONObject(res);

                        String respon= arg0.getString("response");
                        if(respon.equalsIgnoreCase("success"))
                        {

                            service_array=arg0.getJSONArray("services");

                            for(int i=0;i<service_array.length();i++){

                                JSONObject obj = service_array.getJSONObject(i);

//                                listDataHeader.add(new service_getset(obj.getString("id"),obj.getString("gender"),
//                                        obj.getString("service_name"),obj.getString("image")));


                                sub_service_list=new ArrayList<>();

                                if(arg0.has(obj.getString("service_name")))
                                {
                                    JSONArray array = arg0.getJSONArray(obj.getString("service_name"));

                                    if (array.length() > 0) {
                                        for (int j = 0; j < array.length(); j++) {

                                            JSONObject obj1 = array.getJSONObject(j);

                                            sub_service_list.add(new sub_service_getset(obj1.getString("id"), obj1.getString("post_id"),
                                                    obj1.getString("service_name"), obj1.getString("price"), obj1.getString("time")));
                                        }
                                    }

                                }

                                    listDataHeader.add(new service_getset(obj.getString("id"), obj.getString("gender"),
                                            obj.getString("service_name"), obj.getString("image")));

                                listDataChild.put(obj.getString("service_name"),sub_service_list);

                            }

//                            SpinnerAdapter spinnerArrayAdapter = new SpinnerAdapter(getActivity(),
//                                    R.layout.spinner_item,
//                                    service_list);
                           // sp_service.setAdapter(spinnerArrayAdapter);

//                        if(service_list.contains(service_name)){
//                           // sp_service.setSelection(service_list.indexOf(service_name));
//                        }

                            tv_service.setClickable(true);

                           // listdilog();

                        }
                        else if(respon.equalsIgnoreCase("no services")){
                            tv_service.setClickable(false);
                        }
                        else if(respon.equalsIgnoreCase("connection error")){
                            tv_service.setClickable(false);
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        p.dismiss();
                        tv_service.setClickable(false);
                        Log.e("token_exception",e.toString());
                        e.printStackTrace();
                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                p.dismiss();
                Log.e("token_error",e.toString());
                tv_service.setClickable(false);
                if(!nw.isConnectingToInternet())
                    alertDialog();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("gender",pref.getString("gender",""));
                return params;
            }
        };

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    private void get_SubService() {
        // TODO Auto-generated method stub

        final ProgressDialog p = new ProgressDialog(getActivity());

        p.setCancelable(false);

        p.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        p.show();

        String url= Constants.URL+"get_subservices.php";
        Log.e("url", url);
        //   Constants.array=new JSONArray();
        RequestQueue que= Volley.newRequestQueue(getActivity());

        StringRequest obj1=new StringRequest(com.android.volley.Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub

                Log.e("profile_response",res);
                int size=res.length();
                if(size>0)
                {

                    try {

                        JSONObject arg0=new JSONObject(res);

                        //sub_servicelist=new ArrayList<>();

                        String respon= arg0.getString("response");
                        if(respon.equalsIgnoreCase("success"))
                        {

                            subservice_array=arg0.getJSONArray("services");

                            for(int i=0;i<subservice_array.length();i++){

                                JSONObject obj = subservice_array.getJSONObject(i);

                            }

                        }
                        else if(respon.equalsIgnoreCase("no services")){

                        }
                        else if(respon.equalsIgnoreCase("connection error")){

                        }

                        p.dismiss();

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        p.dismiss();
                        Log.e("token_exception",e.toString());
                        e.printStackTrace();
                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                p.dismiss();
                Log.e("token_error",e.toString());

                if(!nw.isConnectingToInternet())
                    alertDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("postid",postid);
                return params;
            }
        };

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    private void Submitbooking() {
        // TODO Auto-generated method stub

        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setCancelable(true);
        p.setMessage("Please Wait...");
        p.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
           p.show();
      Log.e("gender",pref.getString("gender",""));
        Log.e("st_name",st_name);
        Log.e("st_phone",st_phone);
        Log.e("st_service_id",st_service_id);
        Log.e("st_subservice_id",st_subservice_id);
        Log.e("st_date",st_date);
        Log.e("st_time",st_time);
        Log.e("st_message",st_message);



        String url= Constants.URL+"book_appointment.php";
        Log.e("url", url);
        //   Constants.array=new JSONArray();
        RequestQueue que= Volley.newRequestQueue(getActivity());

        StringRequest obj1=new StringRequest(com.android.volley.Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub
                p.dismiss();

                Log.e("profile_response",res);
                int size=res.length();
                if(size>0)
                {

                    try {

                        JSONObject arg0=new JSONObject(res);

                        //sub_servicelist=new ArrayList<>();

                        String respon= arg0.getString("response");
                        if(respon.equalsIgnoreCase("success")) {

//                            Toast.makeText(getActivity(),"Booking Success",Toast.LENGTH_SHORT).show();

                            exitdialog("Your Appointment Booked Succussfully");

                            service_name="";

                            et_name.setText("");
                            et_phone.setText("");
                            //et_email.setText("");
                            et_date.setText("");
                            et_time.setText("");
                            et_message.setText("");
                            tv_service.setText("Select Service");

                        }
                        else if(respon.equalsIgnoreCase("can't book")){

                            Toast.makeText(getActivity(),"Can't Book Appointment",Toast.LENGTH_SHORT).show();
                            exitdialog("Your Appointment Can't Book! Please Try Again");
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        p.dismiss();
                        Log.e("token_exception",e.toString());
                        e.printStackTrace();
                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                p.dismiss();
                Log.e("token_error",e.toString());
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("gender",pref.getString("gender",""));
                params.put("name",st_name);
                params.put("phone",st_phone);
                params.put("serviceid",st_service_id);
                params.put("subserviceid",st_subservice_id);
                params.put("date",st_date);
                params.put("time",st_time);
                params.put("message",st_message);
                return params;
            }
        };

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    public void datedilog()
    {
        final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.datepicker_dialog);
        final DatePicker dp=(DatePicker)dialog.findViewById(R.id.datePicker);

        TextView tv_cancel=(TextView)dialog.findViewById(R.id.tv_cancel);
        TextView tv_save=(TextView)dialog.findViewById(R.id.tv_save);

        dialog.show();

        dp.setMinDate(System.currentTimeMillis() - 1000);
//        dp.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int month=dp.getMonth();
                int day=dp.getDayOfMonth();

                String mon,dy;

                mon=month+"";
                dy=day+"";

                if(month < 10){
                    mon = "0" + month;
                }
                if(day < 10){
                    dy  = "0" + day;
                }

                Log.e("date",dp.getYear()+"-"+mon+"-"+dy);
                String org_dt=dp.getYear()+"-"+mon+"-"+dy;

                org_dt=formatDate(dp.getYear(),Integer.valueOf(mon), Integer.valueOf(dy));

                st_date=org_dt;

                et_date.setText(org_dt);

                dialog.dismiss();
            }
        });
    }

    public void timedilog()
    {
        final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.timepicker_dialog);
        final TimePicker dp=(TimePicker) dialog.findViewById(R.id.timePicker);

        TextView tv_cancel=(TextView)dialog.findViewById(R.id.tv_cancel);
        TextView tv_save=(TextView)dialog.findViewById(R.id.tv_save);
        dialog.show();



        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int hour=dp.getCurrentHour();
                int minute=dp.getCurrentMinute();

                String ti=getTime(hour,minute);
                Log.e("time",ti);

                st_time=ti;

                et_time.setText(ti);

                dialog.dismiss();
            }
        });
    }

    private String getTime(int hr,int min) {
        Time tme = new Time(hr,min,0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("hh:mm a");

        String tm=formatter.format(tme);
        if(tm.contains("am"))
            tm= tm.replaceFirst("am","AM");
        else if(tm.contains("pm"))
            tm= tm.replaceFirst("pm","PM");

        return tm;
    }

    private static String formatDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.YEAR, year);
        Date myDate = cal.getTime();

        String date=new SimpleDateFormat("dd-MMM-yyyy").format(myDate);


        return date;
    }

    public void alertDialog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("No Internet Connection");
        alertDialogBuilder.setPositiveButton("Refresh", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getService();
            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();
    }

    public void listdilog()
    {
        final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.expandablelistview_dialog);
        dialog.show();

        expListView=(ExpandableListView)dialog.findViewById(R.id.expanablelistview);

        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

                //service_name= listDataHeader.get(groupPosition).getServicename();

//                Toast.makeText(getActivity(),
//                        listDataHeader.get(groupPosition).getServicename() + " Expanded",
//                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {

//                Toast.makeText(getActivity(),
//                        listDataHeader.get(groupPosition).getServicename() + " Collapsed",
//                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                // TODO Auto-generated method stub

                service_name= listDataHeader.get(groupPosition).getServicename();
                subservice_name=listDataChild.get(
                        listDataHeader.get(groupPosition).getServicename()).get(
                        childPosition).getSub_ser_name();

                st_service_id= listDataHeader.get(groupPosition).getId();
                st_subservice_id=listDataChild.get(
                        listDataHeader.get(groupPosition).getServicename()).get(
                        childPosition).getId();

                tv_service.setText(service_name+" - "+subservice_name);

                dialog.dismiss();

//                Toast.makeText(
//                        getActivity(),
//                        listDataHeader.get(groupPosition).getServicename()
//                                + " : "
//                                + listDataChild.get(
//                                listDataHeader.get(groupPosition).getServicename()).get(
//                                childPosition).getSub_ser_name(), Toast.LENGTH_SHORT)
//                        .show();

                return false;
            }
        });

    }


    public void exitdialog(String msg){

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(msg);

        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1)
            {

                dialog.dismiss();

            }

        });

//        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                dialog.cancel();
//            }
//        });

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();

    }


}
