package com.dashingdiva.salon.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dashingdiva.salon.R;
import com.dashingdiva.salon.utils.AnimateFirstDisplayListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by surender on 4/4/2017.
 */
public class Pager_Image_Fragment extends Fragment {

    View v;
    ImageView imageView;
    String url;

    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.pager_item_fragment, container, false);

        Log.e("image_fragment","call");

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        Log.e("call","pager_image_framgent");

        imageView=(ImageView)v.findViewById(R.id.imageView);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            url = bundle.getString("url", " ");

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
            imageLoader.getInstance().displayImage(url, imageView, options, animateFirstListener);

        }

        return v;
    }

}
